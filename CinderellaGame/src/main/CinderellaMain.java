package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

public class CinderellaMain extends JFrame implements ChangePanel{
	
	ImageIcon img;
	JPanel mainPanel;
	JPanel helpPanel;
	JPanel winnPanel;
	
	public CinderellaMain(){
		//при нажатии на крестик останавливает процесс
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(200, 100, 700, 600);
		setLayout(null);
		setResizable(false);
		setTitle("Cinderella Game");
		mainPanel = new JPanel();
		mainPanel.setBounds(0, 0, 700, 600);


        img = new ImageIcon(CinderellaMain.class.getResource("/images/AschenputtelTitle.jpg"));

        JLabel mainImage = new JLabel();
        mainImage.setIcon(img);
        mainImage.setBounds(0, -55, 700, 700);

        helpPanel = new JPanel();
        JTextArea helpTextArea = new JTextArea();
        helpTextArea.setFont(new Font("Arial", 2, 16));
        helpTextArea.setText("In this game you must make all tasks to go Bull");
        helpTextArea.setBounds(0, 0, 700, 600);
        helpPanel.add(helpTextArea);
        JButton back = new JButton("Zuruk");
        back.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setContentPane(mainPanel);
                revalidate();
            }
        });
        helpPanel.add(back);
        
        winnPanel = new JPanel();
        winnPanel.setBounds(0, 0, 700, 600);
        winnPanel.setLayout(null);
        winnPanel.setBackground(Color.BLUE);
        JLabel winnLabel = new JLabel();
        winnLabel.setBounds(0,0,700,600);
        winnLabel.setIcon(new ImageIcon(CinderellaMain.class.getResource("/images/winnBall.jpg")));
        
        JButton toMain = new JButton("TO MAIN MENU");
        toMain.setBounds(50, 10, 150, 40);
        toMain.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				changeTo("main");		
			}
		});
        
        winnPanel.add(toMain);
        winnPanel.add(winnLabel);

        JButton start = new JButton("START");
        start.setBounds(200, 200, 100, 50);
        start.setLayout(null);
        start.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
            	initFirstPanel();
            }
        });
        JButton help = new JButton("HELP");
        help.setBounds(200, 260, 100, 50);
        help.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
                setContentPane(helpPanel);
                revalidate();
            }
        });
        JButton exit = new JButton("EXIT");
        exit.setBounds(200, 320, 100, 50);
        exit.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent arg0) {
            	System.exit(0);
            }
        });

        mainPanel.add(start);
        mainPanel.add(help);
        mainPanel.add(exit);
        mainPanel.add(mainImage);
        mainPanel.revalidate();
        setContentPane(mainPanel);////////////////////////////
        setVisible(true);
	}

	public static void main(String[] args) {
		new CinderellaMain();
	}
	
	private void initFirstPanel(){
		CinderellaFirstRoom cinderellaPanel =  new CinderellaFirstRoom(this);
    	cinderellaPanel.setBounds(0, 0, 700, 600);
        setContentPane(cinderellaPanel);
        repaint();
	}

	@Override
	public void changeTo(String to) {
		if(to.equals("main")){
			if(mainPanel != null){
				setContentPane(mainPanel);
			}
		}else if(to.equals("second room")){
			setContentPane(new CinderellaKitchenRoom(this));
		}else if(to.equals("winnPanel")){
			setContentPane(winnPanel);
		}
	}

}
