package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.Rectangle;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.TimerTask;
import java.util.Timer;

import javax.swing.ImageIcon;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.Border;
import javax.swing.border.LineBorder;

/**
 * Created by Urma on 8-Jun 15.
 */
public class CinderellaFirstRoom extends JPanel implements MouseListener{

    JLabel background, shypyrgy, chainik, piaz, stulchik;
    Rectangle selectedArea;
    Rectangle shypyrgyEmptyArea = new Rectangle(87, 183, 50, 230);
    Rectangle chainikEmptyArea = new Rectangle(284, 234, 50, 50);
    Rectangle piazEmptyArea = new Rectangle(480, 94, 50, 100);
    Rectangle stulchikEmptyArea = new Rectangle(245, 467, 100, 100);
    
    JTextField time;
    JTextArea description;
    JLabel selectedThing;
    
    Timer timer;
    
    ChangePanel change;
    
    int gameTime = 15;
    int thingsInPlace = 0;

    public CinderellaFirstRoom(ChangePanel pan){    	
    	change = pan;
        initialisation();        
    }

    /**
     * 
     */
    public void initialisation(){       	
        setLayout(null);
        setBackground(Color.BLUE);
        addMouseListener(this);
        
        background = new JLabel();
        background.setBounds(0, -17, 700,600);        
        background.setIcon(getIcon("/images/cinderella_room1_2.png"));
        
        description = new JTextArea();
        description.setBounds(50,0, 600, 50);
        description.setBorder(new LineBorder(Color.decode("#efefef"), 10));
        description.setEditable(false);
        description.setText("����� ������ ���� �������� ������ ������� ��������� ������� � ���� �������");
        description.setBackground(Color.decode("#efefef"));
        
        
        shypyrgy = new JLabel("Just test");
        shypyrgy.setBounds(177, 153, 50, 230);
        shypyrgy.setIcon(getIcon("/images/shypyrgy2.png"));
        
        chainik = new JLabel();
        chainik.setBounds(284, 334, 50, 50);
        chainik.setIcon(getIcon("/images/chainek2.png"));
        
        piaz = new JLabel();
        piaz.setBounds(480, 294, 50, 100);
        piaz.setIcon(getIcon("/images/piaz2.png"));
        
        stulchik = new JLabel();
        stulchik.setBounds(445, 467, 100, 100);
        stulchik.setIcon(getIcon("/images/stulchik2.png"));
        
        add(shypyrgy);
        add(chainik);
        add(piaz);
        add(stulchik);
        
        time = new JTextField();
        time.setBounds(0,0, 50, 50);
        time.setBackground(Color.decode("#dadafa"));
        time.setEditable(false);
        time.setFont(new Font("Arial", 1, 20));
        time.setText(""+gameTime);
        
        add(time);
        add(description);
        timer = new Timer();
        TimerTask task = new MyTask();
        timer.schedule(task, 0, 1000);
        
        add(background);
    }
    
    public void borderThis(JLabel label){
    	Border borderVisible = new LineBorder(Color.YELLOW);
    	
    	chainik.setBorder(null);
    	stulchik.setBorder(null);
    	shypyrgy.setBorder(null);
    	piaz.setBorder(null);
    	
    	label.setBorder(borderVisible);
    }
    
    private void timeMinus(){
    	if(gameTime <= 0){
    		timer.cancel();
    		change.changeTo("main");
    	}
    	gameTime--;
    	time.setText(""+gameTime);
    }
    
    class MyTask extends TimerTask{
		@Override
		public void run() {
			timeMinus();
		}    	
    }
    
    public void thinkInPlace(){
    	thingsInPlace++;
    	if(thingsInPlace == 4){
    		change.changeTo("second room");
    		timer.cancel();
    	}
    }

	@Override
	public void mouseClicked(MouseEvent me) {
		selectedArea = new Rectangle(me.getX(), me.getY(), 5, 5);
		if(shypyrgy.getBounds().intersects(selectedArea)){
			selectedThing = shypyrgy;
		}else if(chainik.getBounds().intersects(selectedArea)){
			selectedThing = chainik;
		}else if(piaz.getBounds().intersects(selectedArea)){
			selectedThing = piaz;
		}else if(stulchik.getBounds().intersects(selectedArea)){			
			selectedThing = stulchik;
		}		
		if(selectedThing != null){
			borderThis(selectedThing);
			if(selectedArea.intersects(shypyrgyEmptyArea)){
				if(selectedThing == shypyrgy){
					shypyrgy.setBounds(shypyrgyEmptyArea);
					shypyrgyEmptyArea.setBounds(0,0,0,0);
					thinkInPlace();
				}
			}else if(selectedArea.intersects(chainikEmptyArea)){
				if(selectedThing == chainik){
					chainik.setBounds(chainikEmptyArea);
					chainikEmptyArea.setBounds(0,0,0,0);
					thinkInPlace();
				}
			}else if(selectedArea.intersects(piazEmptyArea)){
				if(selectedThing == piaz){
					piaz.setBounds(piazEmptyArea);
					piazEmptyArea.setBounds(0,0,0,0);
					thinkInPlace();
				}
			}else if(selectedArea.intersects(stulchikEmptyArea)){
				if(selectedThing == stulchik){
					stulchik.setBounds(stulchikEmptyArea);
					stulchikEmptyArea.setBounds(0,0,0,0);
					thinkInPlace();
				}
			}
		}		
		
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {}

	@Override
	public void mouseExited(MouseEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent me) {		
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {}
	
	private ImageIcon getIcon(String path){
		return new ImageIcon(CinderellaFirstRoom.class.getResource(path));
	}
}
