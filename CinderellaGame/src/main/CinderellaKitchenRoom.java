package main;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.border.LineBorder;

public class CinderellaKitchenRoom extends JPanel {
	
	ChangePanel change;
	JLabel background;
	JList productsList = new JList();
	
	int gameTime = 20;
	
	JTextField time = new JTextField();
	JTextArea descriptionArea = new JTextArea();
	Timer timer = new Timer();
	
	DefaultListModel<String> productsModel = new DefaultListModel<String>();
	JScrollPane productsScroll = new JScrollPane();
	String products[] = new  String[]{"sol", "perez", "makarony", "ogurzy", "manka"};
	String goodEat[] = new  String[]{"sol", "perez", "makarony","manka", "ogurzy"};
	ArrayList <String> food = new ArrayList<String>();
	
	JButton add = new JButton("ADD");
	
	public CinderellaKitchenRoom(ChangePanel pan){
		change = pan;
		initialisation();
	}
	
	private void initialisation(){
		setLayout(null);
		
		background = new JLabel();
		background.setBounds(0, 0, 700,600);
		background.setIcon(getIcon("/images/kitchenRoom2.png"));
		
		productsList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		productsList.setModel(productsModel);
		
		descriptionArea.setBounds(60,0,400,40);;
		descriptionArea.setEditable(false);
		descriptionArea.setBorder(new LineBorder(Color.WHITE, 10));
		descriptionArea.setText("You must cook porridge on prescription!");
		add(descriptionArea);
		
		time.setText(""+gameTime);
		time.setBounds(0 ,0, 50,50);
		time.setFont(new Font("Arial", 1, 20));
		time.setEditable(false);
		add(time);
		
		TimerTask task = new MyTask();
		timer.schedule(task, 0, 1000);
		
		for(String pro:products)
		productsModel.addElement(pro);
		productsScroll.setBounds(500, 0, 100, 100);
		productsScroll.setViewportView(productsList);
		
		add.setBounds(500, 130, 100, 40);
		add.addActionListener(new ActionListener() {			
			@Override
			public void actionPerformed(ActionEvent e) {
				if(productsModel.size() > 0){
					food.add(productsModel.getElementAt(productsList.getSelectedIndex()));
					productsModel.remove(productsList.getSelectedIndex());
					if(productsModel.size() == 0){
						if(isEatGoot()){
							change.changeTo("winnPanel");
							timer.cancel();
							JOptionPane.showMessageDialog(getParent(), "GRATULATIONS YOU WINN!!!");
						}else{
							JOptionPane.showMessageDialog(getParent(), "This recipe is false");
							food.clear();
							for(String pro:products)
								productsModel.addElement(pro);
						}
					}
				}
			}
		});
		
		add(add);
		add(productsScroll);
		add(background);
	}
	
	private boolean isEatGoot(){		
		for(int i = 0; i < food.size(); i++){
			if(!food.get(i).equals(goodEat[i])){
				return false;
			}
		}
		return true;
	}
	
	class MyTask extends TimerTask{
		@Override
		public void run() {
			if(gameTime<=0){
				change.changeTo("main");
				timer.cancel();
			}
			gameTime--;
			time.setText(""+gameTime);
		}    	
    }
	
	private ImageIcon getIcon(String path){
		return new ImageIcon(CinderellaKitchenRoom.class.getResource(path));
	}
}
